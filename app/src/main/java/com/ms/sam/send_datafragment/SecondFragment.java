package com.ms.sam.send_datafragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    private Button send_button;


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_second, container, false);

        Bundle b=getArguments();
        final String name=  b.getString("name");
        final String contact= b.getString("contact");

        send_button=view.findViewById(R.id.send);

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ThirdFragment frg=new ThirdFragment();

                Bundle bundle=new Bundle();

                bundle.putString("name",name);
                bundle.putString("contact",contact);

                frg.setArguments(bundle);

                FragmentManager fm=getFragmentManager();

                FragmentTransaction ft=fm.beginTransaction();

                ft.replace(R.id.main_page,frg);
                ft.commit();


            }
        });




        return view;
    }

}
