package com.ms.sam.send_datafragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static com.ms.sam.send_datafragment.R.id.textView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdFragment extends Fragment {

    private TextView textView_data;


    public ThirdFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_third, container, false);

        textView_data=view.findViewById(R.id.textView);


        Bundle b=getArguments();
        final String name=  b.getString("name");
        final String contact= b.getString("contact");

        textView_data.setText(name+"\n"+contact);



        return view;
    }

}
