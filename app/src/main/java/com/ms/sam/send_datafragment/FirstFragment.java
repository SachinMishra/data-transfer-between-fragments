package com.ms.sam.send_datafragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

   private   EditText et_name,et_contact;
   private Button send_button;



    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_first, container, false);

        et_contact=view.findViewById(R.id.contactID);
        et_name=view.findViewById(R.id.nameID);

        send_button=view.findViewById(R.id.sendID);

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String name=et_name.getText().toString();
                String contact=et_contact.getText().toString();

                SecondFragment frg=new SecondFragment();

                Bundle bundle=new Bundle();

                bundle.putString("name",name);
                bundle.putString("contact",contact);

                frg.setArguments(bundle);

                FragmentManager fm=getFragmentManager();

                FragmentTransaction ft=fm.beginTransaction();

                ft.replace(R.id.main_page,frg);

                ft.commit();



            }
        });


        return view;
    }

}
